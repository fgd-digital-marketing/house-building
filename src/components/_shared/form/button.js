import React from 'react'

const Button = ({children, className = '', classChild, ...props}) => {
  return (
    <button
      {...props}
      className={`px-2 py-2 flex justify-center items-center w-full gap-2 text-base font-semibold ${className}`}
    >
      <div className={`flex items-center justify-center ${classChild} `}>
        {children}
      </div>
    </button>
  )
}

export default React.memo(Button)