import React from 'react';
import { Page, Text, View, Document, StyleSheet,} from '@react-pdf/renderer';

const styles = StyleSheet.create({
  page: {
    fontFamily: 'Helvetica',
    paddingTop: 30,
    paddingLeft: 60,
    paddingRight: 60,
    paddingBottom: 30,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  invoiceSection: {
    fontSize: 12,
    textAlign:'left',
    marginBottom: 10,
    flexDirection:'row',
    width:'100%',
  },
  invoiceGrid:{
    width:'50%',
  },
  invoiceDetails: {
    paddingVertical: 4,
  },
  table: {
    display: 'table',
    width: 'auto',
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
  },
  // style Row
  tableRowHeader: {
    borderBottomWidth:1,
    margin: 0,
    flexDirection: 'row',
    fontSize:10,
    backgroundColor:'#BDBDBD',
  },
  tableRow: {
    borderBottomWidth:1,
    margin: 0,
    flexDirection: 'row',
    fontSize:8,
  },
  tableRowTotal: {
    borderBottomWidth:1,
    fontSize:8,
    width:'100%',
    margin: 0,
    flexDirection: 'row',
    justifyContent:'space-between',
    backgroundColor:'#BDBDBD',
  },
  // style coloum  
  cellQuantity:{
    borderRightWidth:1,
    width:'15%',
    height:'100%',
    paddingVertical:2,
    alignItems: 'center',
    textAlign: 'center',
  },
  cellDescription:{
    borderRightWidth:1,
    width:'45%',
    height:'100%',
    paddingVertical:2,
    alignItems:'center',
    textAlign: 'center',
  },
  cellUnitPrice:{
    borderRightWidth:1,
    width:'15%',
    height:'100%',
    paddingVertical:2,
    alignItems: 'center',
    textAlign: 'center',
  },
  cellAmount:{
    width:'25%',
    height:'100%',
    paddingVertical:2,
    alignItems: 'center',
    textAlign: 'center',
  },
  cellTotal:{
    width:'75%',
    height:'100%',
    paddingVertical:2,
    alignItems: 'center',
    textAlign: 'center',
  },
  tableCellHeader: {
    margin: 5,
    width:'25%',
    fontSize: 12,
    fontWeight: 'bold',
    borderBottomColor: '#bfbfbf',
    borderBottomWidth: 1,
    alignItems: 'center',
    textAlign: 'center',
  },
  tableCell: {
    margin: 5,
    fontSize: 10,
    textAlign: 'center',
  },
  totalAmount: {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  description:{
    flex: 'flex',
    justifyContent: 'justify'
  }
});

const PDFComponent = ({ invoiceData }) => {
  const { invoiceNumber, date, customerName, items, totalAmount } = invoiceData;

  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <Text style={styles.title}>Purchase Order</Text>
        <View style={styles.invoiceSection}>
          <View style={styles.invoiceGrid}>
            <Text style={styles.invoiceDetails} > Delivery Address </Text>
            <Text style={styles.invoiceDetails} > Street Address </Text>
            <Text style={styles.invoiceDetails} > Town/City </Text>
            <Text style={styles.invoiceDetails} > Country </Text>
            <Text style={styles.invoiceDetails} > Postcode </Text>
          </View> 
          <View style={styles.invoiceGrid}>
            <Text style={styles.invoiceDetails} > Delivery Address </Text>
            <Text style={styles.invoiceDetails} > Street Address </Text>
            <Text style={styles.invoiceDetails} > Town/City </Text>
            <Text style={styles.invoiceDetails} > Country </Text>
            <Text style={styles.invoiceDetails} > Postcode </Text>
          </View>
        </View>
        <View style={styles.invoiceSection}>
          <Text style={styles.invoiceGrid}>
            <Text style={styles.invoiceDetails}> PO No : {customerName} </Text>
          </Text>
          <Text style={styles.invoiceGrid}>
            <Text style={styles.invoiceDetails}> awokaojdo {customerName} </Text>
          </Text>
        </View>

        {/* table layout */}
        <View style={styles.table}>
          <View style={styles.tableRowHeader}>
            <Text style={styles.cellQuantity}>Quantity</Text>
            <Text style={styles.cellDescription}>Description</Text>
            <Text style={styles.cellUnitPrice}>Unit Price</Text>
            <Text style={styles.cellAmount}>Total Price</Text>
          </View>
          {items?.map((item) => (
            <View key={item.id} style={styles.tableRow}>
              <Text style={styles.cellQuantity}>{item.quantity}</Text>
              <Text style={styles.cellDescription}>{item.name}</Text>
              <Text style={styles.cellUnitPrice}>{item.price}</Text>
              <Text style={styles.cellAmount}>{item.price * item.quantity}</Text>
            </View>
          ))}
          <View style={styles.tableRowTotal}>
            <Text style={styles.cellTotal}>Total</Text>
            <Text style={styles.cellAmount}>{totalAmount}</Text>
          </View>
        </View>

        {/* Bottom section */}
          <View style={styles.invoiceSection}>
            <View style={styles.invoiceGrid}>
              <Text style={styles.invoiceDetails} > Delivery Address </Text>
              <Text style={styles.invoiceDetails} > Street Address </Text>
              <Text style={styles.invoiceDetails} > Town/City </Text>
              <Text style={styles.invoiceDetails} > Country </Text>
              <Text style={styles.invoiceDetails} > Postcode </Text>
            </View> 
            <View style={styles.invoiceGrid}>
              <Text style={styles.invoiceDetails} > Delivery Date </Text>
              <Text style={styles.invoiceDetails} >  </Text>
              <Text style={styles.invoiceDetails} > Payment Terms </Text>
              <Text style={styles.invoiceDetails} >  </Text>
              <Text style={styles.invoiceDetails} >  </Text>
            </View>
          </View>

          <View style={styles.invoiceSection}>
            <View style={styles.invoiceGrid}>
              <Text style={styles.invoiceDetails} > Authorised by </Text>
            </View> 
            <View style={styles.invoiceGrid}>
              <Text style={styles.invoiceDetails} > Date: </Text>
            </View>
          </View>
      </Page>
    </Document>
  );
};

export default PDFComponent;
