import React from "react";
import Button from "../form/button";

export function ListMaterial({ selectedCategory, items, setItems }) {
  //grouping data awal 
  const groupedData = items.reduce((acc, item) => {
    acc[item.category] = [...(acc[item.category] || []), item];
    return acc;
  }, {});
  //memfilter data sesuai filter inputan dari user
  const filteredData = selectedCategory
    ? Object.entries(groupedData).filter(
        ([category, items]) => category === selectedCategory
      )
    : Object.entries(groupedData);
  //jika user memasukan inputan secara langsung
  const handleQuantityChange = (id, value) => {
    const parsedValue = parseInt(value, 10);

    if (isNaN(parsedValue) || parsedValue < 0 ) {
      setItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id ? { ...item, quantity: 0  } : item
      )
    );  
      return ; // Tidak mengizinkan nilai negatif atau NaN (not a number)
    }

    setItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id ? { ...item, quantity: parsedValue } : item
      )
    );
  };
  
  //function menambahkan 1 quantity ketika button di klik
  const handleQuantityIncrease = (id) => {
    setItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id ? { ...item, quantity: item.quantity + 1 } : item
      )
    );
  };
  //function mengurangi quantity ketika button di klik
 const handleQuantityDecrease = (id) => {
  setItems((prevItems) =>
    prevItems.map((item) =>
      item.id === id
        ? { ...item, quantity: item.quantity > 0 ? item.quantity - 1 : 0 }
        : item
    )
  );
};

  return (
    <>
      {filteredData.map(([category, items]) => (
        <div key={category} className="flex flex-col py-4 w-full ">
          <h1 className="font-semibold text-base text-black px-4">
            Building {category}
          </h1>
          {items.map((item) => (
            <div key={item.id} className=" flex px-4 py-4 gap-2 border-b">
              <div
                className="rounded-lg border w-[80px] h-[80px] bg-contain bg-center bg-no-repeat"
                style={{
                  backgroundImage: `url(${item.image})`,
                }}
              ></div>
              <div className="flex flex-grow justify-between">
                <div className="flex flex-col justify-between">
                  <h2 className="font-semibold text-gray-1">{item.name}</h2>
                  <div>
                    <h3 className="text-xs text-gray-2">Rp {item.price}/pcs</h3>
                    <p className="text-[10px] text-gray-3">
                      Stock: {item.stock}{" "}
                    </p>
                  </div>
                </div>
                <div className="flex items-center ">
                  <div className="flex">
                    <Button
                      className="px-1 py-1 border rounded-tl-lg rounded-bl-lg"
                      onClick={() => handleQuantityDecrease(item.id)}
                    >
                      <svg
                        width="16"
                        height="16"
                        viewBox="0 0 16 16"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clipPath="url(#clip0_172_1012)">
                          <path
                            d="M12 8.66667H4.00004C3.63337 8.66667 3.33337 8.36667 3.33337 8C3.33337 7.63333 3.63337 7.33333 4.00004 7.33333H12C12.3667 7.33333 12.6667 7.63333 12.6667 8C12.6667 8.36667 12.3667 8.66667 12 8.66667Z"
                            fill="#333333"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_172_1012">
                            <rect width="16" height="16" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </Button>
                    <input
                      type="text"
                      className=" w-[40px] px-[10px] border text-center text-xs placeholder-black appearance-none focus:outline-none disabled:bg-white"
                      value={item.quantity}
                      onChange={(e) =>
                        handleQuantityChange(item.id, e.target.value)
                      }
                    />
                    <Button
                      className="px-1 py-1 border rounded-tr-lg rounded-br-lg"
                      onClick={() => handleQuantityIncrease(item.id)}
                    >
                      <svg
                        width="16"
                        height="16"
                        viewBox="0 0 16 16"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clipPath="url(#clip0_172_1018)">
                          <path
                            d="M12 8.66667H8.66671V12C8.66671 12.3667 8.36671 12.6667 8.00004 12.6667C7.63337 12.6667 7.33337 12.3667 7.33337 12V8.66667H4.00004C3.63337 8.66667 3.33337 8.36666 3.33337 8C3.33337 7.63333 3.63337 7.33333 4.00004 7.33333H7.33337V4C7.33337 3.63333 7.63337 3.33333 8.00004 3.33333C8.36671 3.33333 8.66671 3.63333 8.66671 4V7.33333H12C12.3667 7.33333 12.6667 7.63333 12.6667 8C12.6667 8.36666 12.3667 8.66667 12 8.66667Z"
                            fill="#333333"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_172_1018">
                            <rect width="16" height="16" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      ))}
    </>
  );
}
