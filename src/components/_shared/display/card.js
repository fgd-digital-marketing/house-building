import React from 'react'

const Card = ({children, className='', ...props}) => {
  return (
    <div className={`w-full p-6 rounded-lg shadow-md flex flex-col items-center space-y-3 ${className}`}>
      {children}
    </div>
  )
}

export default Card