import React from "react";
import Button from "./form/button";

const Header = ({ children, leftIcon, rightIcon, rightOnClick, ...props }) => {
  return (
    <div className="w-full flex items-center border-t-2 border-b-2 px-6 py-3 relative">
      <div className="absolute left-6 flex items-center">
        <Button {...props}>
          {leftIcon}
        </Button>
      </div>
      <div className="w-full flex justify-center">
        <h1 className="font-montserrat font-bold text-base text-dark">
          {children}
        </h1>
      </div>
      <div className="absolute right-6 flex items-center">
        <Button onClick={rightOnClick}>
          {rightIcon}
        </Button>
      </div>
    </div>
  );
};

export default Header;
