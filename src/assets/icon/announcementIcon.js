import React from "react";

export default function AnnouncementIcon({ fillColor, width, height }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 16 16"
      fill="none"
    >
      <path
        d="M13.3333 1.33331H2.66659C1.93325 1.33331 1.33325 1.93331 1.33325 2.66665V14.6666L3.99992 12H13.3333C14.0666 12 14.6666 11.4 14.6666 10.6666V2.66665C14.6666 1.93331 14.0666 1.33331 13.3333 1.33331ZM13.3333 10.6666H3.44659L2.66659 11.4466V2.66665H13.3333V10.6666ZM7.33325 3.33331H8.66659V7.33331H7.33325V3.33331ZM7.33325 8.66665H8.66659V9.99998H7.33325V8.66665Z"
        fill={fillColor}
      />
    </svg>
  );
}
