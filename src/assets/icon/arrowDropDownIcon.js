import React from "react";

export default function ArrowDropDownIcon({width, height, fillColor}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill={fillColor}
    >
      <g clipPath="url(#clip0_173_768)">
        <path d="M7 10L12 15L17 10H7Z" fill="#333333" />
      </g>
      <defs>
        <clipPath id="clip0_173_768">
          <rect width={width} height={height} fill="none" />
        </clipPath>
      </defs>
    </svg>
  );
}
