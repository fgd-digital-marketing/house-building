import React from "react";

export default function DownloadIcon({width,height,fillColor}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
    >
      <g clipPath="url(#clip0_157_2192)">
        <path
          d="M19 9H15V3H9V9H5L12 16L19 9ZM5 18V20H19V18H5Z"
          fill={fillColor}
        />
      </g>
      <defs>
        <clipPath id="clip0_157_2192">
          <rect width={width} height={height} fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
}
