import React from "react";

export default function FilterIcon({height,width,fillColor}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 20 20"
      fill={fillColor}
    >
      <g clipPath="url(#clip0_173_832)">
        <path
          d="M8.33333 15H11.6667V13.3333H8.33333V15ZM2.5 5V6.66667H17.5V5H2.5ZM5 10.8333H15V9.16667H5V10.8333Z"
          fill="#7367F0"
        />
      </g>
      <defs>
        <clipPath id="clip0_173_832">
          <rect width={width} height={height} fill="none" />
        </clipPath>
      </defs>
    </svg>
  );
}
