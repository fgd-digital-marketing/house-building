import React from "react";

export default function ReceiptIcon({ fillColor, width, height }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 33 32"
      fill="none"
    >
      <g clipPath="url(#clip0_157_1329)">
        <path
          d="M26.3333 4.66663L24.3333 2.66663L22.3333 4.66663L20.3333 2.66663L18.3333 4.66663L16.3333 2.66663L14.3333 4.66663L12.3333 2.66663L10.3333 4.66663L8.33325 2.66663V21.3333H4.33325V25.3333C4.33325 27.5466 6.11992 29.3333 8.33325 29.3333H24.3333C26.5466 29.3333 28.3333 27.5466 28.3333 25.3333V2.66663L26.3333 4.66663ZM25.6666 25.3333C25.6666 26.0666 25.0666 26.6666 24.3333 26.6666C23.5999 26.6666 22.9999 26.0666 22.9999 25.3333V21.3333H10.9999V6.66663H25.6666V25.3333Z"
          fill={fillColor}
        />
        <path
          d="M20.3333 9.33325H12.3333V11.9999H20.3333V9.33325Z"
          fill={fillColor}
        />
        <path
          d="M24.3332 9.33325H21.6665V11.9999H24.3332V9.33325Z"
          fill={fillColor}
        />
        <path
          d="M20.3333 13.3333H12.3333V15.9999H20.3333V13.3333Z"
          fill={fillColor}
        />
        <path
          d="M24.3332 13.3333H21.6665V15.9999H24.3332V13.3333Z"
          fill={fillColor}
        />
      </g>
      <defs>
        <clipPath id="clip0_157_1329">
          <rect
            width="32"
            height="32"
            fill="white"
            transform="translate(0.333252)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}
