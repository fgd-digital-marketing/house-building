import React from "react";
import ReactDOM from "react-dom/client";
import "./assets/css/tailwind.css";
import "./assets/css/custom.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/home";
import CustomerList from "./pages/customer";
import CustomerAdd from "./pages/customer/add";
import { Order } from "./pages/order";
import { Summary } from "./pages/order/summary";
import { Purchase } from "./pages/order/purchase";
import MapsLocation from "./pages/customer/maps";
import Login from "./pages/auth/login";
import PurchasePDF from "./pages/order/purchasePDF";
import ViewPdf from "./pages/order/viewPdf";
import { Provider } from "react-redux";
import Store from "./redux/store";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={Store}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} exact />
        <Route path="/login" element={<Login />} exact />
        <Route path="/customer" element={<CustomerList />} />
        <Route path="/customer/add" element={<CustomerAdd />} />
        <Route path="/customer/maps" element={<MapsLocation />} />
        <Route path="/order" element={<Order />} />
        <Route path="/order/summary" element={<Summary />} />
        <Route path="/order/summary/purchase" element={<Purchase />} />
        <Route path="/order/summary/purchase/view-pdf" element={<ViewPdf/>}/>
      </Routes>
    </BrowserRouter>
  </Provider>
);
