import React, { useEffect, useState } from "react";
import Header from "../../components/_shared/header";
import Button from "../../components/_shared/form/button";
import { ListMaterial } from "../../components/_shared/display/listMaterial";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import { useNavigate } from "react-router-dom";
import SearchIcon from "../../assets/icon/searchIcon";
import ArrowDropDownIcon from "../../assets/icon/arrowDropDownIcon";
import FilterIcon from "../../assets/icon/filterIcon";
import image from "../../assets/imagetest.png";
import axios from "axios";
export function Order() {
  const dataMaterial = [
    {
      id: 1,
      category: "Material",
      name: "Baja",
      price: 5,
      stock: 50,
      quantity: 0,
      image: image,
    },
    {
      id: 2,
      category: "Material",
      name: "Batu Bata",
      price: 5,
      stock: 100,
      quantity: 0,
      image: image,
    },
    {
      id: 3,
      category: "Material",
      name: "Pasir",
      price: 500000,
      stock: 30,
      quantity: 0,
      image: image,
    },
    {
      id: 4,
      category: "Material",
      name: "Besi 5m",
      price: 50000,
      stock: 90,
      quantity: 0,
      image: image,
    },
    {
      id: 5,
      category: "Finished",
      name: "door1",
      price: 52300,
      stock: 99,
      quantity: 0,
      image: image,
    },
    {
      id: 6,
      category: "Finished",
      name: "door2",
      price: 51230,
      stock: 80,
      quantity: 0,
      image: image,
    },
    {
      id: 7,
      category: "Finished",
      name: "door3",
      price: 51233,
      stock: 70,
      quantity: 0,
      image: image,
    },
    {
      id: 8,
      category: "Finished",
      name: "door4",
      price: 43565,
      stock: 239,
      quantity: 0,
      image: image,
    },
  ];

  const navigate = useNavigate();
  const [selectedCategory, setSelectedCategory] = useState("");
  const [items, setItems] = useState(dataMaterial);
  const [total, setTotal] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [customer, setCustomer] = useState([])

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_API_URL}/customer/?page_size=50`, {
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => setCustomer(res.data.results))
      .catch((error) => console.log(error));
  }, []);

  useEffect(() => {
    const calculateTotalPrice = () => {
      const totalPrice = items.reduce(
        (total, item) => total + item.price * item.quantity,
        0
      );
      setTotal(totalPrice);
    };

    calculateTotalPrice();
  }, [items]);

  const handleOptionSelect = (customer) => {
    setSelectedCustomer(customer);
    setIsOpen(false);
  };

  const handleSubmit = async () => {
    const selectedItems = items.filter((item) => item.quantity > 0);
    if (selectedItems && selectedItems.length > 0) {
      const currentDate = new Date();
      const dateString = currentDate.toDateString();

      const orderItemsArray = selectedItems.map((item) => ({
        product: item.name,
        quantity: item.quantity,
        price: item.price,
      }));
      const dataFinal = {
        id32: "",
        customer: selectedCustomer.name,
        order_date: dateString,
        approved_by: "",
        order_items: orderItemsArray,
      };
      console.log(dateString);
      navigate("/order/summary/purchase", {
        state: {
          customer: selectedCustomer,
          time: dateString,
          purchase: selectedItems,
        },
      });
    } else {
      alert("silahkan pilih items terlebih dahulu");
    }
  };

  return (
    <div className="w-full max-h-full mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white">
        {/* header */}
        <Header
          onClick={() => window.location.assign("/")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
        >
          Sales order
        </Header>
        {/* customer bar */}
        <div className="py-5">
          <div className="w-full flex flex-col items-center gap-4">
            <div className="w-full text-left font-semibold px-5">Summary</div>
            <div className="w-[90%] ">
              <h3 className="font-semibold">Customer </h3>
              <div className="w-full border p-3 rounded-lg  flex flex-col items-center gap-3">
                {selectedCustomer ? (
                  <div className="w-full flex justify-between items-center">
                    <div
                      className="px-4 py-2 flex items-center gap-3 w-full cursor-pointer"
                      onClick={() => setIsOpen(!isOpen)}
                    >
                      <img
                        alt={selectedCustomer.name}
                        src='/image/profile.png'
                        className="w-10 h-10 bg-primary rounded-full"
                      />
                      <div className="w-full">
                        <h1 className="font-montserrat font-semibold text-base text-gray-1">
                          {selectedCustomer.name}
                        </h1>
                        <h1 className="font-montserrat font-normal text-xs text-[#82868B]">
                          {selectedCustomer.address}
                        </h1>
                      </div>
                    </div>
                    <div>
                      <Button onClick={() => setIsOpen(!isOpen)}>
                        <ArrowDropDownIcon
                          width={24}
                          height={24}
                          fillColor="#333333"
                        />
                      </Button>
                    </div>
                  </div>
                ) : (
                  <div
                    className="w-full flex items-center justify-between cursor-pointer"
                    onClick={() => setIsOpen(!isOpen)}
                  >
                    <h1 className="font-montserrat font-semibold text-base text-gray-1">
                      Select Customer
                    </h1>
                    <div>
                      <Button onClick={() => setIsOpen(!isOpen)}>
                        <ArrowDropDownIcon
                          width={24}
                          height={24}
                          fillColor="#333333"
                        />
                      </Button>
                    </div>
                  </div>
                )}
                {isOpen && (
                  <ul className="w-full flex flex-col">
                    {customer.map((customer) => (
                      <li
                        key={customer.id32}
                        className="px-4 py-2 cursor-pointer hover:bg-blue-100 flex items-center gap-3"
                        onClick={() => handleOptionSelect(customer)}
                      >
                        <img
                          alt={customer.name}
                          src='/image/profile.png'
                          className="w-10 h-10 bg-primary rounded-full"
                        />
                        <div>
                          <h1 className="font-montserrat font-semibold text-base text-gray-1">
                            {customer.name}
                          </h1>
                          <h1 className="font-montserrat font-normal text-xs text-[#82868B]">
                            {customer.address}
                          </h1>
                        </div>
                      </li>
                    ))}
                  </ul>
                )}
              </div>
            </div>
            {/* search bar */}
            <div className="border-t flex flex-col items-center py-2 w-full h-auto">
              <div className="flex w-full items-center gap-2 py-2 px-4 ">
                <div className="w-full flex items-center h-full px-2 py-2 border-[0.5px] border-grey-1 rounded-lg gap-2">
                  <SearchIcon fillColor={"#82868B"} width={20} height={20} />
                  <input
                    className="w-full text-xs focus:outline-none"
                    type="text"
                    placeholder="Search"
                  />
                </div>
                <div className="">
                  <Button className=" border rounded-lg">
                    <FilterIcon width={20} height={20} fill={"#7367F0"} />
                  </Button>
                </div>
              </div>

              {/* material List */}
              <div className="pb-20 w-full">
                <ListMaterial
                  selectedCategory={selectedCategory}
                  items={items}
                  setItems={setItems}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="fixed w-full max-w-md bottom-0 flex-col justify-center px-5 pt-2 pb-7 bg-white">
          <div className={`${total === 0 ? "hidden " : " "} flex-col`}>
            <p className=" text-xs font-medium ">Total</p>
            <h2 className="text-2xl font-bold">RP {total} </h2>
          </div>
          <Button
            onClick={handleSubmit}
            className="w-full bg-primary rounded-lg"
          >
            <h1 className="font-montserrat font-semibold text-sm">
              Add To Cart
            </h1>
          </Button>
        </div>
      </div>
    </div>
  );
}
