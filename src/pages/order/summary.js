import React, { useEffect, useState } from "react";
import Button from "../../components/_shared/form/button";
import Header from "../../components/_shared/header";
import { ListMaterial } from "../../components/_shared/display/listMaterial";
import { useLocation, useNavigate } from "react-router-dom";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import ArrowDropDownIcon from "../../assets/icon/arrowDropDownIcon";

export function Summary() {
  const usenavigate = useNavigate();
  const location = useLocation();
  const [items, setItems] = useState(location?.state?.selectedItems);
  const [total, setTotal] = useState(0);

  console.log(items);
  useEffect(() => {
    // Fungsi untuk menghitung total harga
    const calculateTotalPrice = () => {
      const totalPrice = items.reduce(
        (total, item) => total + item.price * item.quantity,
        0
      );
      setTotal(totalPrice);
    };

    calculateTotalPrice();
  }, [items]);
  
  const handleSubmit = () => {
    const finalPurchase = items.filter((item) => item.quantity > 0);
    if (finalPurchase && finalPurchase.length > 0) {
      usenavigate("/order/summary/purchase", { state: { finalPurchase } });
    } else {
      alert("items di checkout tidak boleh kosong");
    }
  };

  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white">
        {/* header */}
        <Header
          onClick={() => window.location.assign("/order")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
        >
          Sales order
        </Header>
        {/* customer bar */}
        <div className="py-5">
          <div className="flex flex-col items-center gap-4">
            <div className="w-full text-left font-semibold px-5">Summary</div>
            <div className="w-[90%] ">
              <h3 className="font-semibold">Customer </h3>
              <div className="w-full border p-3 rounded-lg  flex items-center gap-3">
                <img
                  alt="profile"
                  src="/image/profile.png"
                  className="w-10 h-10 bg-primary rounded-full"
                />
                <div className="w-full flex justify-between items-center">
                  <div>
                    <h1 className="font-montserrat font-semibold text-base text-gray-1">
                      Jhon Doe
                    </h1>
                    <h1 className="font-montserrat font-normal text-xs text-[#82868B]">
                      testing company
                    </h1>
                  </div>
                  <div>
                    <Button>
                      <ArrowDropDownIcon
                        width={24}
                        height={24}
                        fillColor="#333333"
                      />
                    </Button>
                  </div>
                </div>
              </div>
            </div>

            <div className="border-t flex flex-col items-center py-2 pb-20 w-full h-auto">
              {/* material List */}
              <ListMaterial
                selectedCategory=""
                items={items}
                setItems={setItems}
              />
            </div>
          </div>
        </div>
        <div className="fixed w-full max-w-md bottom-0 flex-col justify-center px-5 pt-5 pb-7 bg-white">
          <div className="flex-col">
            <p className=" text-xs font-medium ">Total</p>
            <h2 className="text-2xl font-bold">RP {total} </h2>
          </div>
          <Button
            onClick={handleSubmit}
            className="w-full bg-primary rounded-lg"
          >
            <h1 className="font-montserrat font-semibold text-sm">
              Add To Cart
            </h1>
          </Button>
        </div>
      </div>
    </div>
  );
}
