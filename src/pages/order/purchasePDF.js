import React from "react";
import { useLocation } from "react-router-dom";
import Header from "../../components/_shared/header";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import html2pdf from "html2pdf.js";

const PurchasePDF = () => {
  const location = useLocation();
  const data = location?.state;
  const totalAmount = data.reduce(
    (total, item) => total + item.quantity * item.price,
    0
  );
  const downloadPDF = () => {
    const element = document.getElementById("purchasePDF");
    const options = {
      margin: 10,
      filename: "purchase.pdf",
      image: { type: "jpeg", quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: { unit: "mm", format: "a4", orientation: "portrait" },
    };

    html2pdf().from(element).set(options).save();
  };
  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white">
        <Header
          onClick={() => window.location.assign("/")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
          rightOnClick={downloadPDF}
          rightIcon={
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_157_2192)">
                <path
                  d="M19 9H15V3H9V9H5L12 16L19 9ZM5 18V20H19V18H5Z"
                  fill="#7367F0"
                />
              </g>
              <defs>
                <clipPath id="clip0_157_2192">
                  <rect width="24" height="24" fill="white" />
                </clipPath>
              </defs>
            </svg>
          }
        >
          Purchase Order
        </Header>
        <div className="p-4" id="purchasePDF">
          <div className="border-2 border-gray-4 rounded-lg p-2">
            <h1 className="font-montserrat font-bold text-lg text-center">
              Purchase Order
            </h1>
            <div className="grid grid-cols-2 gap-2 pt-5">
              <div className="w-full flex gap-2">
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1>Supplier Name</h1>
                  <h1>Street Address</h1>
                  <h1>Town/City</h1>
                  <h1>Country</h1>
                  <h1>Postcode</h1>
                  <div className="pt-5">
                    <h1>PO No.</h1>
                  </div>
                </div>
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                </div>
              </div>
              <div className="w-full flex gap-2">
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1>Buyer Name</h1>
                  <h1>Street Address</h1>
                  <h1>Town/City</h1>
                  <h1>Country</h1>
                  <h1>Postcode</h1>
                  <div className="pt-5">
                    <h1>PO Date</h1>
                  </div>
                </div>
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                </div>
              </div>
            </div>
            <br />
            <table className="w-full border rounded text-center text-sm font-light">
              <thead className="border-b text-xs font-semibold font-montserrat">
                <tr>
                  <th scope="col" className="border-r px-3 py-1">
                    Quantity
                  </th>
                  <th scope="col" className="border-r px-3 py-1">
                    Description
                  </th>
                  <th scope="col" className="border-r px-3 py-1">
                    Unit Price
                  </th>
                  <th scope="col" className="px-3 py-1">
                    Amount
                  </th>
                </tr>
              </thead>
              <tbody>
                {data &&
                  data.map((data, index) => (
                    <tr className="border-b text-xs font-medium font-montserrat" key={index}>
                      <td className="whitespace-nowrap border-r px-3 py-1">
                        {data.quantity}
                      </td>
                      <td className="whitespace-nowrap border-r px-3 py-1"></td>
                      <td className="whitespace-nowrap border-r px-3 py-1">
                        {data.price}
                      </td>
                      <td className="whitespace-nowrap px-3 py-1">
                        {data.quantity * data.price}
                      </td>
                    </tr>
                  ))}
              </tbody>
              <tfoot>
                <tr className="text-xs font-semibold font-montserrat">
                  <td className="border-r px-3 py-1">Total</td>
                  <td className="border-r px-3 py-1" colSpan={2}></td>
                  <td className="px-3 py-1">{totalAmount}</td>
                </tr>
              </tfoot>
            </table>
            <div className="grid grid-cols-2 gap-2 pt-5">
              <div className="w-full flex gap-2">
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1>Delivery Address</h1>
                  <h1>Street Address</h1>
                  <h1>Town/City</h1>
                  <h1>Country</h1>
                  <h1>Postcode</h1>
                  <div className="pt-5">
                    <h1>Authorised by</h1>
                  </div>
                  <br />
                  <h1 className="italic text-[9px]">Original: Supplier / Copy 1: Warehouse / Copy 2: Accounts</h1>
                </div>
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                </div>
              </div>
              <div className="w-full flex gap-2">
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1>Delivery Date</h1>
                  <h1 className="font-normal">20/08/23</h1>
                  <h1>Payment Terms</h1>
                  <br />
                  <br />
                  <div className="pt-5">
                    <h1>Date:</h1>
                  </div>
                </div>
                <div className="text-left text-xs font-semibold font-montserrat">
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                  <h1></h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PurchasePDF;
