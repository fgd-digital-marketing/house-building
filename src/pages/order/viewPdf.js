import React from "react";
import Header from "../../components/_shared/header";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import { PDFViewer, PDFDownloadLink } from "@react-pdf/renderer";
import PDFComponent from "../../components/_shared/PDFComponent";
import Download from "../../assets/icon/downloadIcon";
import { useLocation } from "react-router-dom";

const ViewPdf = () => {
    const location = useLocation();
    const dataMentah = location?.state;

    console.log(dataMentah);
    
    const invoiceData = {
      invoiceNumber: dataMentah?.customer.address,
      date: "2023-07-28",
      customerName: "John Doe",
      items: dataMentah?.selectedItems,
      totalAmount: 65,
    };
  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white">
        {/*Header  */}
        <Header
          onClick={() => window.location.assign("/order")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
            }
          rightIcon={
            <Download fillColor={"#7367F0"} width={24} height={24} />
        }
        >
          Purchase Order
        </Header>
        {/* Menampilkan PDF */}
        <PDFViewer width="100%" height="100%">
          <PDFComponent invoiceData={invoiceData} />
        </PDFViewer>

        {/* Tombol untuk mengunduh PDF */}
        <PDFDownloadLink
          document={<PDFComponent invoiceData={invoiceData} />}
          fileName="invoice.pdf"
        >
          {({ blob, url, loading, error }) =>
            loading ? "Loading document..." : "Download Invoice"
          }
        </PDFDownloadLink>
      </div>
    </div>
  );
};

export default ViewPdf;
