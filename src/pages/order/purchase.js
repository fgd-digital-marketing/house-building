import React, { useState, useEffect } from "react";
import Header from "../../components/_shared/header";
import Button from "../../components/_shared/form/button";
import { useLocation, useNavigate } from "react-router-dom";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";

export function Purchase() {
  const location = useLocation();
  const navigate = useNavigate();
  const customer = location?.state?.customer;
  const time = location?.state?.time;
  const selectedItems = location?.state?.purchase;
  const [total, setTotal] = useState(0);

 console.log(location?.state);
  useEffect(() => {
    // Fungsi untuk menghitung total harga
    const calculateTotalPrice = () => {
      const totalPrice = selectedItems.reduce(
        (total, item) => total + item.price * item.quantity,
        0
      );
      setTotal(totalPrice);
    };

    calculateTotalPrice();
  }, [selectedItems]);

  const handleGeneratePdf = () => {
    navigate("/order/summary/purchase/view-pdf", {
      state: {customer:customer,date:time ,selectedItems},
    })
  }
  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto pb-20 bg-white">
        <Header
          onClick={() => window.location.assign("/")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
        >
          Purchase Order
        </Header>
        <div className="flex flex-col gap-4 items-center p-4">
          <h1 className="text-2xl font-bold text-gray-1">Complete</h1>
          <div className="flex flex-col gap-2 border rounded-lg w-full p-4 ">
            <div className="flex flex-col">
              <div className="flex w-full justify-between text-xs text-gray-3">
                <p className="font-semibold">{customer.name}</p>
                <p>{time}</p>
              </div>
              <h1 className="text-sm font-semibold text-gray-1">Company Customer</h1>
            </div>
            <div className="flex w-full justify-between text-xs font-semibold">
              <p>Summary</p>
              <p>Price</p>
            </div>
            <div className="flex flex-col gap-4">
              {selectedItems.map((item) => (
                <div key={item.id} className="flex flex-col border-b py-2">
                  <h1 className="text-sm font-bold">{item.name}</h1>
                  <div className="flex justify-between text-xs font-semibold">
                    <p className="font-normal w-[30%]">{item.price}/pcs</p>
                    <p>{item.quantity}x</p>
                    <p>Rp {item.quantity * item.price}</p>
                  </div>
                  <p className="text-[10px] font-normal text-gray-3">
                    stock: 135
                  </p>
                </div>
              ))}
              <div className="flex flex-col text-right">
                <p className="text-xs text-gray-3 font-medium">Total</p>
                <h1 className="text-2xl text-gray-1 font-bold">Rp {total}</h1>
              </div>
            </div>
          </div>
          <div className="flex flex-col gap-2 w-full ">
            <Button
              className="border rounded-lg"
              classChild="w-full justify-start px-2 "
              onClick={handleGeneratePdf}
            >
              <div className="flex w-full gap-4 justify-start items-center text-sm text-gray-1 font-bold ">
                <svg
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_157_2155)">
                    <path
                      d="M13.3333 1.33334H5.33334C4.60001 1.33334 4.00001 1.93334 4.00001 2.66668V10.6667C4.00001 11.4 4.60001 12 5.33334 12H13.3333C14.0667 12 14.6667 11.4 14.6667 10.6667V2.66668C14.6667 1.93334 14.0667 1.33334 13.3333 1.33334ZM7.66668 6.33334C7.66668 6.88668 7.22001 7.33334 6.66668 7.33334H6.00001V8.66668H5.00001V4.66668H6.66668C7.22001 4.66668 7.66668 5.11334 7.66668 5.66668V6.33334ZM11 7.66668C11 8.22001 10.5533 8.66668 10 8.66668H8.33334V4.66668H10C10.5533 4.66668 11 5.11334 11 5.66668V7.66668ZM13.6667 5.66668H12.6667V6.33334H13.6667V7.33334H12.6667V8.66668H11.6667V4.66668H13.6667V5.66668ZM6.00001 6.33334H6.66668V5.66668H6.00001V6.33334ZM2.66668 4.00001H1.33334V13.3333C1.33334 14.0667 1.93334 14.6667 2.66668 14.6667H12V13.3333H2.66668V4.00001ZM9.33334 7.66668H10V5.66668H9.33334V7.66668Z"
                      fill="#7367F0"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_157_2155">
                      <rect width="16" height="16" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                <p>See in PDF</p>
              </div>
            </Button>
            <Button
              className="border rounded-lg"
              classChild="w-full justify-start px-2"
            >
              <div className="flex w-full gap-4 justify-start items-center text-sm text-gray-1 font-bold ">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                >
                  <g clipPath="url(#clip0_157_2160)">
                    <path
                      d="M12.6667 6H10V2H6.00001V6H3.33334L8.00001 10.6667L12.6667 6ZM3.33334 12V13.3333H12.6667V12H3.33334Z"
                      fill="#7367F0"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_157_2160">
                      <rect width="16" height="16" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                <p>Download PDF</p>
              </div>
            </Button>
            <Button
              className="border rounded-lg"
              classChild="w-full justify-start px-2"
            >
              <div className="flex w-full gap-4 justify-start items-center text-sm text-gray-1 font-bold ">
                <svg
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_157_2165)">
                    <path
                      d="M12.6667 5.33333H3.33334C2.22668 5.33333 1.33334 6.22667 1.33334 7.33333V11.3333H4.00001V14H12V11.3333H14.6667V7.33333C14.6667 6.22667 13.7733 5.33333 12.6667 5.33333ZM10.6667 12.6667H5.33334V9.33333H10.6667V12.6667ZM12.6667 8C12.3 8 12 7.7 12 7.33333C12 6.96667 12.3 6.66667 12.6667 6.66667C13.0333 6.66667 13.3333 6.96667 13.3333 7.33333C13.3333 7.7 13.0333 8 12.6667 8ZM12 2H4.00001V4.66667H12V2Z"
                      fill="#7367F0"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_157_2165">
                      <rect width="16" height="16" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                <p>Print PDF</p>
              </div>
            </Button>
          </div>
        </div>
        <div className="fixed w-full max-w-md bottom-0 flex-col justify-center px-5 pt-5 pb-7 bg-white">
          <Button className="w-full bg-primary rounded-lg">
            <h1 className="font-montserrat font-semibold text-sm">Done</h1>
          </Button>
        </div>
      </div>
    </div>
  );
}
