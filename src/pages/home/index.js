import React from "react";
import Card from "../../components/_shared/display/card";
import Button from "../../components/_shared/form/button";
import CartIcon from "../../assets/icon/cartIcon";
import ReceiptIcon from "../../assets/icon/receiptIcon";
import GroupIcon from "../../assets/icon/groupIcon";
import AppIcon from "../../assets/icon/appIcon";
import AnnouncementIcon from "../../assets/icon/announcementIcon";
import NotificationIcon from "../../assets/icon/notificationIcon";
export default function Home() {
  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white">
        <div className="p-5 space-y-5 w-full">
          <div className="flex justify-between items-center">
            <div className="w-full">
              <h1 className="font-montserrat font-bold text-base text-gray-1">
                Hello, Jhon Doe
              </h1>
              <h1 className="font-montserrat font-normal text-sm text-black">
                Sales
              </h1>
            </div>
            <div className="flex items-center gap-6">
              <NotificationIcon fillColor={"#7367F0"} width={24} height={25} />
              <div className="w-10 h-10">
                <img
                  src="/image/profile.png"
                  className="rounded-full bg-black/80"
                  alt="Profile"
                />
              </div>
            </div>
          </div>
          <Card>
            <h1 className="font-montserrat font-bold text-xl text-primary">
              Quick Customer Purchase
            </h1>
            <div className="w-full flex justify-between items-center border-gray-4">
              <div className="flex items-center gap-3">
                <img
                  src="/image/profile.png"
                  className="w-10 h-10 rounded-full bg-primary/80"
                  alt="Profile"
                />
                <div className="w-full">
                  <h1 className="font-montserrat font-semibold text-base text-gray-1">
                    Jhon Doe
                  </h1>
                  <h1 className="font-montserrat font-normal text-xs text-black">
                    J&D CO
                  </h1>
                </div>
              </div>
              <div>
                <Button className="rounded-full text-primary border-[1px] border-primary px-3 py-0.5">
                  Choose
                </Button>
              </div>
            </div>
            <Button
              onClick={() => window.location.assign("/order")}
              className="w-full px-2 py-2.5 bg-primary rounded-lg"
            >
              <CartIcon fillColor={"#FFFFFF"} width={"20"} height={"20"} />
              <h1 className="font-montserrat font-semibold text-sm ml-1.5">
                Add Purchase
              </h1>
            </Button>
            <Button
              onClick={() => window.location.assign("/customer")}
              className="w-full bg-transparent border-[1px] border-primary rounded-lg text-primary"
            >
              <h1 className="font-montserrat font-semibold text-sm">
                Add Customer
              </h1>
            </Button>
          </Card>
          <div className="flex flex-col gap-2">
            <div className="w-full flex justify-between items-center">
              <div className="flex items-center gap-2">
                <AnnouncementIcon
                  fillColor={"#7367F0"}
                  width={16}
                  height={16}
                />
                <h1 className="font-montserrat font-bold text-base text-dark">
                  Quick Notification
                </h1>
              </div>
              <div className="">
                <h1 className="font-montserrat font-semibold text-xs text-primary">
                  View More
                </h1>
              </div>
            </div>
          </div>
          <Card className="border-[1px] border-primary">
            <div className="w-full flex justify-between items-center">
              <h1 className="font-montserrat font-semibold text-sm text-dark">
                Lorem Ipsum
              </h1>
              <h1 className="font-montserrat font-normal text-[10px] text-dark">
                23 Februari 2022
              </h1>
            </div>
            <h1 className="font-montserrat font-normal text-[10px] text-dark">
              Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. Sunt
              qui esse pariatur duis deserunt mollit dolore
            </h1>
          </Card>
          <div className="flex flex-col gap-2">
            <h1 className="font-montserrat font-bold text-base text-dark">
              Self-Service
            </h1>
            <div className="grid grid-cols-4 gap-2">
              <div className="p-2 space-y-2">
                <Button className="bg-[#BB6BD9]/30 rounded-lg">
                  <CartIcon fillColor={"#BB6BD9"} width={32} height={32} />
                </Button>
                <h1 className="font-montserrat font-medium text-xs text-dark text-center">
                  Customer PO
                </h1>
              </div>
              <div className="p-2 space-y-2">
                <Button className="bg-blue-1/30 rounded-lg">
                  <ReceiptIcon fillColor={"#2F80ED"} width={32} height={32} />
                </Button>
                <h1 className="font-montserrat font-medium text-xs text-dark text-center">
                  Sales Order
                </h1>
              </div>
              <div className="p-2 space-y-2">
                <Button className="bg-green-1/30 rounded-lg">
                  <GroupIcon fillColor={"#219653"} width={32} height={32} />
                </Button>
                <h1 className="font-montserrat font-medium text-xs text-dark text-center">
                  Customer List
                </h1>
              </div>
              <div className="p-2 space-y-2">
                <Button className="bg-dark/30 rounded-lg">
                  <AppIcon fillColor={"#4B4B4B"} width={32} height={32} />
                </Button>
                <h1 className="font-montserrat font-medium text-xs text-dark text-center">
                  More
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
