import React, { useEffect, useState } from "react";
import Button from "../../components/_shared/form/button";
import Header from "../../components/_shared/header";
import { useLocation, useNavigate } from "react-router-dom";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import { connect, useDispatch, useSelector } from "react-redux";
import { postCustomer } from "../../redux/action/customer.action";

const CustomerAdd = (props) => {
  const navigate = useNavigate();
  const stateData = useLocation();
  const dispatch = useDispatch();
  const { isSuccess, errmessage } = useSelector((state) => state.data);
  const [name, setName] = useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [companyProfile, setCompanyProfile] = useState("");
  const address = stateData?.state?.location?.address;
  const location = `${stateData?.state?.location?.longitude}, ${stateData?.state?.location?.latitude}`;
  const [isSubmit, setSubmit] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(
      postCustomer({
        name: name,
        contact_number: contactNumber,
        company_profile: companyProfile,
        address: address,
        location: location,
      })
    );
    setSubmit(true);
  };

  useEffect(() => {
    if (isSubmit) {
      if (isSuccess) {
        navigate("/customer");
      } else {
        alert("Gagal Memasukan Data");
        setSubmit(false);
      }
    }
  }, [isSuccess, errmessage]);

  useEffect(() => {
    const nameValue = stateData?.state?.user.name;
    const contactNumberValue = stateData?.state?.user.contactNumber;
    const companyProfileValue = stateData?.state?.user.companyProfile;
    if (nameValue && contactNumberValue && companyProfileValue) {
      setName(nameValue);
      setContactNumber(contactNumberValue);
      setCompanyProfile(companyProfileValue);
    }
  }, [stateData]);

  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white">
        <Header
          onClick={() => window.location.assign("/customer")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
        >
          Customer Add
        </Header>
        <div className="px-5 py-4">
          <div className="space-y-4">
            <div className="flex flex-col gap-1">
              <label>Customer Name</label>
              <input
                className="w-full focus:outline-none border-2 px-3 py-2 rounded-lg"
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                placeholder="Choose type"
              />
            </div>
            <div className="flex flex-col gap-1">
              <label>Customer Contact</label>
              <input
                className="w-full focus:outline-none border-2 px-3 py-2 rounded-lg"
                type="text"
                value={contactNumber}
                onChange={(e) => setContactNumber(e.target.value)}
                placeholder="Choose Year"
              />
            </div>
            <div className="flex flex-col gap-1">
              <label>Customer Company</label>
              <input
                className="w-full focus:outline-none border-2 px-3 py-2 rounded-lg"
                type="text"
                value={companyProfile}
                onChange={(e) => setCompanyProfile(e.target.value)}
                placeholder="Choose Year"
              />
            </div>
            <div className="flex flex-col gap-1">
              <label>Location</label>
              <Button
                className="px-5 py-3.5 text-primary rounded-lg border-2 border-primary"
                onClick={() =>
                  navigate("/customer/maps", {
                    state: {
                      user: {
                        name: name,
                        contactNumber: contactNumber,
                        companyProfile: companyProfile,
                      },
                    },
                  })
                }
              >
                Insert by GPS
              </Button>
            </div>
            <div className="flex flex-col gap-1">
              <label>Description Company</label>
              <textarea
                className="w-full focus:outline-none border-2 px-3 py-2 rounded-lg"
                type="text"
                placeholder="Eg. your text here"
              />
            </div>
          </div>
        </div>
        <div className="fixed w-full max-w-md bottom-0 flex-col justify-center px-5 pt-5 pb-7 bg-white">
          <Button
            onClick={handleSubmit}
            className="bg-primary w-full rounded-lg"
          >
            <h1 className="font-montserrat font-semibold text-sm">Submit</h1>
          </Button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.data,
  };
};

export default connect(mapStateToProps)(CustomerAdd);
