import React from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import { useEffect, useState } from "react";
import Button from "../../components/_shared/form/button";
import { useLocation, useNavigate } from "react-router-dom";
import Header from "../../components/_shared/header";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import axios from "axios";

const MapsLocation = () => {
  const navigate = useNavigate();
  const dataState = useLocation();
  const [map, setMap] = useState(null);
  const [userLocation, setUserLocation] = useState(null);

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          axios
            .get(
              `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&zoom=18&addressdetails=1`
            )
            .then((res) => {
              setUserLocation({
                address: res.data.display_name,
                latitude: latitude,
                longitude: longitude,
              });
            });
        },
        (error) => {
          console.error("Error getting user location:", error);
        },
        { enableHighAccuracy: true }
      );
    } else {
      console.error("Geolocation is not supported by this browser.");
    }
  }, []);

  const mapStyles = {
    height: "100vh",
    width: "100%",
  };

  const defaultCenter = { lat: 0, lng: 0 };

  const onLoad = (map) => {
    setMap(map);
  };

  return (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen bg-white mx-auto">
        <Header
          onClick={() => window.location.assign("/customer/add")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
        >
          Add Customer
        </Header>
        <LoadScript googleMapsApiKey="AIzaSyCKE9S_OfBoXpdIlnNEfwbmJNaYGnS4lls">
          <GoogleMap
            mapContainerStyle={mapStyles}
            center={userLocation || defaultCenter}
            zoom={15}
            onLoad={onLoad}
          >
            {userLocation && <Marker position={userLocation} />}
          </GoogleMap>
        </LoadScript>
        <div className="fixed w-full max-w-md bottom-0 flex-col justify-center px-5 pt-5 pb-7 bg-white">
          <Button
            onClick={() =>
              navigate("/customer/add", {
                state: {
                  location: userLocation,
                  user: dataState?.state?.user
                },
              })
            }
            className="w-full bg-primary rounded-lg"
          >
            <h1 className="font-montserrat font-semibold text-sm">
              Select Location
            </h1>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default MapsLocation;
