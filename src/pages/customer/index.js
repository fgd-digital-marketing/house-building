import React, { useEffect, useState } from "react";
import Header from "../../components/_shared/header";
import Button from "../../components/_shared/form/button";
import AddIcon from "../../assets/icon/addIcon";
import MoreVertIcon from "../../assets/icon/moreVertIcon";
import SearchIcon from "../../assets/icon/searchIcon";
import LeftArrowIcon from "../../assets/icon/leftArrowIcon";
import { useLocation, useNavigate } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { getCustomer } from "../../redux/action/customer.action";

const CustomerList = (props) => {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const queryParams = new URLSearchParams(location.search);
  const page = queryParams.get("page") || 1;

  useEffect(() => {
    props.loadcustomer();
    dispatch(getCustomer(page));
  }, [page]);
  return props.data.loading ? (
    <div className="w-full h-screen flex justify-center items-center">
      Loading..
    </div>
  ) : (
    <div className="w-full min-h-screen mx-auto bg-gray-100">
      <div className="max-w-md min-h-screen mx-auto bg-white relative">
        <Header
          onClick={() => window.location.assign("/")}
          leftIcon={
            <LeftArrowIcon fillColor={"#7367F0"} width={24} height={24} />
          }
        >
          Customer List
        </Header>
        <div className="px-5 py-4">
          <div className="w-full flex items-center border-[0.5px] border-[#82868B] p-2 rounded-lg gap-2">
            <SearchIcon fillColor={"#82868B"} width={20} height={20} />
            <input
              className="w-full focus:outline-none"
              type="text"
              placeholder="Search"
            />
          </div>
          <div className="pb-12">
            {props.data?.customer?.results &&
              props.data?.customer?.results.map((costumer, index) => (
                <div
                  key={index}
                  className="w-full p-6 rounded-lg shadow-md flex items-center gap-3"
                >
                  <img
                    alt="Profile"
                    src="/image/profile.png"
                    className="w-10 h-10 bg-primary rounded-full"
                  />
                  <div className="w-full flex justify-between items-center">
                    <div>
                      <h1 className="font-montserrat font-semibold text-base text-gray-1">
                        {costumer.name}
                      </h1>
                      <h1 className="font-montserrat font-normal text-xs text-black">
                        {costumer.company_profile}
                      </h1>
                    </div>
                    <div>
                      <Button>
                        <MoreVertIcon
                          fillColor={"#4F4F4F"}
                          width={24}
                          height={24}
                        />
                      </Button>
                    </div>
                  </div>
                </div>
              ))}
          </div>
          <div className="flex gap-4 items-center justify-between">
            <Button
              className="bg-primary disabled:bg-primary/80"
              disabled={page <= 1}
              onClick={() => navigate(`?page=${parseInt(page) - 1}`)}
            >
              Prev
            </Button>
            <Button
              className="bg-primary disabled:bg-primary/80"
              disabled={page == props.data?.customer?.total_pages}
              onClick={() => navigate(`?page=${parseInt(page) + 1}`)}
            >
              Next
            </Button>
          </div>
        </div>
        {/* Float Add Customer */}
        <div className="w-full max-w-md flex justify-end px-4">
          <div className="fixed bottom-4">
            <Button
              onClick={() => window.location.assign("/customer/add")}
              className="bg-primary rounded-full py-4 px-4 w-full"
            >
              <AddIcon fillColor={"#EDEDED"} width={32} height={32} />
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadcustomer: () => dispatch(getCustomer()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerList);
