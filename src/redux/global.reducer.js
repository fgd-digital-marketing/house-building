import { CUSTOMER_CREATE, CUSTOMER_LIST, FAIL_REQUEST, MAKE_REQUEST } from "./action/action.type";

const initialState = {
  loading: true,
}

export const globalReducer = (state = initialState, action) => {
  switch (action.type) {
    case MAKE_REQUEST:
      return {
        ...state,
        loading: true
      }
    case FAIL_REQUEST:
      return {
        ...state,
        loading: false,
        isSuccess: false,
        errmessage: action.payload
      }
    case CUSTOMER_LIST:
      return {
        ...state,
        loading: false,
        isSuccess: true,
        customer: action.payload
      }
    case CUSTOMER_CREATE:
      return {
        ...state,
        loading: false,
        isSuccess: true,
        customer: action.payload
      }
    default:
      return state;
  }
}