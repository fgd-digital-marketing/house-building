import axios from "axios";
import { CUSTOMER_CREATE, CUSTOMER_LIST } from "./action.type";
import { failRequest, makeRequest } from "./request.action";

export const listCustomer = (data) => {
  return {
    type: CUSTOMER_LIST,
    payload: data,
  };
};

export const addCustomer = (data) => {
  return {
    type: CUSTOMER_CREATE,
    payload: data
  };
};

export const getCustomer = (page) => {
  return (dispatch) => {
    dispatch(makeRequest());
    axios
      .get(`${process.env.REACT_APP_API_URL}/customer/?page=${page}`, {
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        dispatch(listCustomer(res.data));
      })
      .catch((err) => {
        dispatch(failRequest(err.message));
      });
  };
};

export const postCustomer = (data) => {
  return (dispatch) => {
    dispatch(makeRequest());
    axios
      .post(`${process.env.REACT_APP_API_URL}/customer/`, data, {
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        dispatch(addCustomer(res.data));
      })
      .catch((err) => {
        dispatch(failRequest(err.message));
      });
  };
};
