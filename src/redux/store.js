import { configureStore, combineReducers } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { globalReducer } from "./global.reducer";

const rootReducer = combineReducers({data: globalReducer})
const Store=configureStore({reducer: rootReducer, middleware:[thunk, logger]})

export default Store;