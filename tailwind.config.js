/** @type {import('tailwindcss').Config} */
module.exports = {
  mode:'jit',
  content: ["./src/**/*.{js,jsx}"],
  theme: {
    extend: {
      colors: {
        base: "#F6F5FB",
        dark: "#4B4B4B",
        light: "#BABFC7",
        primary: "#7367F0",
        blue: {
          1: "#2F80ED",
        },
        gray: {
          1: "#333333",
          2: "#4F4F4F",
          3: "#828282",
          4: "#BDBDBD",
          5: "#E0E0E0",
          6: "#F2F2F2",
        },
        green: {
          1: "#219653",
        },
        red: {
          1: "#BD2A2A",
        },
        neutral: {
          10: "#FFFFFF",
          30: "#EDEDED",
          40: "#E0E0E0",
          80: "#616161",
          90: "#404040",
        },
      },
      fontFamily: {
        montserrat: ["Montserrat", "sans-serif"],
      },
    },
  },
  plugins: [],
};
